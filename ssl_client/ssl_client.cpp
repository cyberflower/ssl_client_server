#include <stdio.h> // for perror
#include <stdlib.h> // for exit
#include <string.h> // for memset
#include <unistd.h> // for close
#include <arpa/inet.h> // for htons
#include <netinet/in.h> // for sockaddr_in
#include <sys/socket.h> // for socket
#include <thread>
#include <mutex>

#include <errno.h>
#include <malloc.h>
#include <resolv.h>
#include <netdb.h>
#include "openssl/ssl.h"
#include "openssl/err.h"

#define FAIL    -1

using namespace std;

void usage(){
    printf("syntax : ssl_client <host> <port>\n");
    printf("sample : ssl_client 127.0.0.1 1234\n");
}

int OpenConnection(const char *hostname, int port){
	int sd;
	struct hostent *host;
	struct sockaddr_in addr;
	if ( (host = gethostbyname(hostname)) == NULL ){
		perror(hostname);
		abort();
	}
	sd = socket(PF_INET, SOCK_STREAM, 0);
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = *(long*)(host->h_addr);
	if ( connect(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0 ){
		close(sd);
		perror(hostname);
		abort();
	}
	return sd;
}

SSL_CTX* InitCTX(void){
	SSL_METHOD *method;
	SSL_CTX *ctx;
	OpenSSL_add_all_algorithms();  /* Load cryptos, et.al. */
	SSL_load_error_strings();   /* Bring in and register error messages */
	method = (SSL_METHOD*)TLSv1_2_client_method();  /* Create new client-method instance */
	ctx = SSL_CTX_new(method);   /* Create new context */
	if ( ctx == NULL ){
		ERR_print_errors_fp(stderr);
		abort();
	}
	return ctx;
}

void ShowCerts(SSL* ssl){
	X509 *cert;
	char *line;
	cert = SSL_get_peer_certificate(ssl); /* get the server's certificate */
	if ( cert != NULL ){
		printf("Server certificates:\n");
		line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
		printf("Subject: %s\n", line);
		free(line);       /* free the malloc'ed string */
		line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
		printf("Issuer: %s\n", line);
		free(line);       /* free the malloc'ed string */
		X509_free(cert);     /* free the malloc'ed certificate copy */
	}
	else printf("Info: No client certificates configured.\n");
}

void client_send(SSL *ssl, mutex &m){
	while(true){
		const static int BUFSIZE = 1024;
		char buf[BUFSIZE];
		m.lock();
		scanf("%s", buf);
		if (strcmp(buf, "quit") == 0){
			printf("[+] Process terminated\n");
			exit(0);
		}
		m.unlock();
		ssize_t sent = SSL_write(ssl, buf, strlen(buf));
		if (sent == 0) {
			perror("send failed");
			exit(0);
		}
	}
}

void client_recv(SSL *ssl, mutex &m){
	while(true){
		const static int BUFSIZE = 1024;
		char buf[BUFSIZE];
		ssize_t received = SSL_read(ssl, buf, BUFSIZE - 1);
		if (received == 0 || received == -1) {
			printf("[!] server disconnected\n");
			exit(0);
		}
		if(strcmp("quit",buf)==0){
			printf("[!] server disconnected\n");
			exit(0);
		}
		buf[received] = '\0';
		printf("%s\n", buf);
	}
}

int main(int argc, char **argv){
    if(argc!=3){
        usage();
        return 1;
    }

	SSL_library_init();
	char *hostname=argv[1];
	char *portnum=argv[2];
	SSL_CTX *ctx = InitCTX();
	int server = OpenConnection(hostname, atoi(portnum));
	SSL *ssl = SSL_new(ctx);      /* create new SSL connection state */
	SSL_set_fd(ssl, server);    /* attach the socket descriptor */
	if ( SSL_connect(ssl) == FAIL ){   /* perform the connection */
		ERR_print_errors_fp(stderr);
		return -1;
	}
	printf("connected\n");
	mutex m;
	thread send_thread=thread(client_send,ssl,ref(m));
	thread recv_thread=thread(client_recv,ssl,ref(m));

	send_thread.join();
	recv_thread.join();

	close(server);
    return 0;
}
