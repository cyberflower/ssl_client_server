#include <stdio.h> // for perror
#include <iostream>
#include <stdlib.h> // for exit
#include <string.h> // for memset
#include <unistd.h> // for close
#include <arpa/inet.h> // for htons
#include <netinet/in.h> // for sockaddr_in
#include <sys/socket.h> // for socket
#include <thread>
#include <deque>
#include <map>
#include <mutex>
#include <signal.h> // for keyboard interrupt

#include <errno.h>
#include <malloc.h>
#include <sys/types.h>
#include <resolv.h>
#include "openssl/ssl.h"
#include "openssl/err.h"
#define FAIL    -1

using namespace std;
const int SZ=1005; // maximum size of thread

thread client_thread[SZ];
map<int,SSL*> child_exist;
int visit[SZ];
deque<int> unused_thread_idx;

void usage(){
    printf("syntax : ssl_server <port> [-b]\n");
    printf("sample : ssl_server 1234 -b\n");
}

void terminate(int sig){
	printf("\n[!] Close server\n");
	for(int i=1;i<SZ;i++){
		char end_message[]="quit";
		if(child_exist.find(i)!=child_exist.end()) SSL_write(child_exist[i],end_message,strlen(end_message));
	}	
	exit(0);
}

// Create the SSL socket and intialize the socket address structure
int OpenListener(int port){
	int sd;
	struct sockaddr_in addr;
	sd = socket(PF_INET, SOCK_STREAM, 0);
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = INADDR_ANY;
	if (bind(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0 ){
		perror("can't bind port");
		abort();
	}
	if ( listen(sd, 10) != 0 ){
		perror("Can't configure listening port");
		abort();
	}
	return sd;
}

int isRoot(){
	return getuid()==0?1:0;
}

SSL_CTX* InitServerCTX(void){
	SSL_METHOD *method;
	SSL_CTX *ctx;
	OpenSSL_add_all_algorithms();  /* load & register all cryptos, etc. */
	SSL_load_error_strings();   /* load all error messages */
	method = (SSL_METHOD*)TLSv1_2_server_method();  /* create new server-method instance */
	ctx = SSL_CTX_new(method);   /* create new context from method */
	if ( ctx == NULL ){
		ERR_print_errors_fp(stderr);
		abort();
	}
	return ctx;
}

void LoadCertificates(SSL_CTX* ctx, char* CertFile, char* KeyFile){
	/* set the local certificate from CertFile */
	if ( SSL_CTX_use_certificate_file(ctx, CertFile, SSL_FILETYPE_PEM) <= 0 ){
		ERR_print_errors_fp(stderr);
		abort();
	}
	/* set the private key from KeyFile (may be the same as CertFile) */
	if ( SSL_CTX_use_PrivateKey_file(ctx, KeyFile, SSL_FILETYPE_PEM) <= 0 ){
		ERR_print_errors_fp(stderr);
		abort();
	}
	/* verify private key */
	if ( !SSL_CTX_check_private_key(ctx) ){
		fprintf(stderr, "Private key does not match the public certificate\n");
		abort();
	}
}

void ShowCerts(SSL* ssl){
	X509 *cert;
	char *line;
	cert = SSL_get_peer_certificate(ssl); /* Get certificates (if available) */
	if ( cert != NULL ){
		printf("Server certificates:\n");
		line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
		printf("Subject: %s\n", line);
		free(line);
		line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
		printf("Issuer: %s\n", line);
		free(line);
		X509_free(cert);
	}
	//else printf("No certificates.\n");
}

void recv_send(int sockfd, bool broad_cast, int idx, SSL *ssl, mutex &m){
	while(true){
		const static int BUFSIZE = 1024;
		char buf[BUFSIZE];

		if(SSL_accept(ssl)==FAIL) {
			printf("[-] SSL connection errors\n");
			return;
		}
		ShowCerts(ssl);
		ssize_t received = SSL_read(ssl, buf, BUFSIZE - 1);
		if (received == 0 || received == -1) {
			m.lock();
			printf("[!] Process %d terminated\n",idx);
			child_exist.erase(idx);
			m.unlock();
			return;
		}
		buf[received] = '\0';
		printf("%s\n", buf);
		
		if(strcmp(buf,"quit")==0){
			m.lock();
			printf("[!] Process %d terminated\n",idx);
			child_exist.erase(idx);
			m.unlock();
			return;
		}
		if(!broad_cast){
			ssize_t sent = SSL_write(ssl, buf, strlen(buf));
			m.lock();
			if (sent == 0) {
				printf("[!] Process %d terminated\n",idx);
				child_exist.erase(idx);
				return;
			}
			m.unlock();		
		}
		else{
			for(int i=0;i<SZ;i++){
				if(child_exist.find(i)==child_exist.end()) continue;
				ssize_t sent = SSL_write(child_exist[i], buf, strlen(buf));
				m.lock();
				if(sent==0){
					perror("send failed");
					child_exist.erase(idx);
					return;				
				}			
				m.unlock();
			}
		}	
	}
}

int main(int argc, char **argv){
	bool broad_cast=false;
    if(argc==2 || argc==3){
		if(argc==3 && strcmp("-b",argv[2])==0){
			cout<<"broad cast on"<<'\n';
			broad_cast=true;
		}
		else if(argc==2) broad_cast=false;
		else{
			usage();
			return -1;
		}
	}
	else{
		usage();
		return -1;
	}

	if(!isRoot()){
		printf("[-] This program must be run by root/sudo user.\n");
		return -1;
	}

	// Initialize the SSL library
	SSL_library_init();
	char* portnum = argv[1];
	SSL_CTX *ctx = InitServerCTX();        /* initialize SSL */
	LoadCertificates(ctx, "test.com.pem", "test.com.pem"); /* load certs */
	int server = OpenListener(atoi(portnum));    /* create server socket */

	for(int i=1;i<SZ;i++) unused_thread_idx.push_back(i);
	mutex m;
	signal(SIGINT, terminate);
	while (!unused_thread_idx.empty()) {
		struct sockaddr_in addr;
		socklen_t clientlen = sizeof(sockaddr);
		SSL *ssl;
		int client = accept(server, reinterpret_cast<struct sockaddr*>(&addr), &clientlen);
		if (client < 0) {
			perror("ERROR on accept");
			break;
		}
		printf("connected\n");
		ssl = SSL_new(ctx);              /* get new SSL state with context */
		SSL_set_fd(ssl, client);      /* set connection socket to SSL state */

		int idx=unused_thread_idx.front(); unused_thread_idx.pop_front(); 
		child_exist[idx]=ssl;
		client_thread[idx]=thread(recv_send,server,broad_cast,idx,ssl,ref(m));
		visit[idx]=1;
		//unused_thread_idx.push_back(idx);
	}
	for(int i=1;i<SZ;i++){
		if(visit[i]) client_thread[i].join();
	}
	close(server);
    return 0;
}
